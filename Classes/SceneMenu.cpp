//
// Created by rion on 02.03.21.
// menu with button START PLAY

#include "SceneMenu.h"
#include "SceneGame.h"
//#include "SimpleAudioEngine.h"
#include <iostream>

USING_NS_CC;
using namespace ProgrammingTools;
using namespace std;

Scene* SceneMenu::createScene()
{
    return SceneMenu::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in SceneMenu.cpp\n");
}

// on "init" you need to initialize your instance
bool SceneMenu::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
            "CloseNormal.png",
            "CloseSelected.png",
            CC_CALLBACK_1(SceneMenu::menuCloseCallback, this));

    if (closeItem == nullptr ||
        closeItem->getContentSize().width <= 0 ||
        closeItem->getContentSize().height <= 0)
    {
        problemLoading("'CloseNormal.png' and 'CloseSelected.png'");
    }
    else
    {
        float x = origin.x + visibleSize.width - closeItem->getContentSize().width/2;
        float y = origin.y + closeItem->getContentSize().height/2;
        closeItem->setPosition(Vec2(x,y));
    }

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);


    Snakes* snakes;

    //background
    //Sprite* sprite = Sprite::create("SceneMenuBackground.jpg");
    background = makeCenterOfSpriteWithSize("SceneMenuBackground.jpg", visibleSize);
    if (background == nullptr)
    {
        problemLoading("'SceneMenuBackground.jpg'");
    }
    else
    {
        background->setAnchorPoint(Vec2(0, 0));
        // position the background on the center of the screen
        background->setPosition(Vec2(origin.x, origin.y));
        // add the background as a child to this layer
        this->addChild(background, 0);
    }

    //
    // fades out the background in 2 seconds

    auto fadeOut = FadeOut::create(2.0f);
    Sprite* mySprite = Sprite::create("SkinStartBTN.png");

    //mySprite->runAction(fadeOut);
    mySprite->setPosition(Vec2(200,200));
    //this->addChild(mySprite, 2);
    //


    auto listener1 = EventListenerTouchOneByOne::create();

 //   mySprite->addChild(listener1);

    listener1->onTouchBegan = [](Touch* touch, Event* event){
    // your code
    return true; // if you are consuming it
};

    //menu
    auto startItem = MenuItemImage::create(
            "SkinStartBTN.png",
            "SkinStartBTN.png",
            CC_CALLBACK_1(SceneMenu::menuCloseCallback, this));

    if (startItem == nullptr ||
        startItem->getContentSize().width <= 0 ||
        startItem->getContentSize().height <= 0)
    {
        problemLoading("'SkinStartBTN.png' and 'SkinStartBTN.png'");
    }
    else
    {
        float x = origin.x + visibleSize.width/2 ;//- startItem->getContentSize().width/2;
        float y = origin.y + visibleSize.height/1.5 ;//+ startItem->getContentSize().height/2;
        startItem->setPosition(Vec2(x,y));
    }

    auto menu2 = Menu::create(startItem, NULL);
    menu2->setPosition(Vec2::ZERO);
    //this->addChild(menu2, 1);

    //label
    auto label = Label::createWithTTF("Start", "fonts/Marker Felt.ttf", 24);
    //label->updateDisplayedColor(Color3B(255,255,210));
    if (label == nullptr)
    {
        problemLoading("'fonts/Marker Felt.ttf'");
    }
    else
    {
        // position the label on the center of the screen
        label->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height/1.5));// - label->getContentSize().height));

        // add the label as a child to this layer
        this->addChild(label, 3);
    }
    //we have label and need make button with equal size

////////////////////////

    //
    startButton = cocos2d::ui::Button::create("SkinStartBTN.png", "SkinStartBTN_Press.png", "SkinStartBTN.png", ui::Widget::TextureResType::LOCAL);
    //startButton->setColor(Color3B(155, 155, 155)); //colour is set to red as suppose to.
    //make button completely transparent before display
    //startButton->runAction(FadeOut::create(0.0f));
    //startButton->setOpacity(120);
    //label->setOpacity(255);
    //stat dimming background

    auto delayBackground = DelayTime::create(1.0f);

    background->runAction(DelayTime::create(1.0f));

    auto fadeBackground = FadeTo::create(1.0f, 210);
    auto seqBackground = Sequence::create(delayBackground, fadeBackground, nullptr);
    //
    auto fadeSkinButton = FadeTo::create(1.7f, 100);
    auto fadeLabelButton = FadeTo::create(1.7f, 255);
    auto seqButton = Sequence::create(fadeSkinButton, fadeBackground, nullptr);

    //background->runAction(FadeTo::create(1.0f, 210));
    auto delay_1 = DelayTime::create(0.5f);
    auto delay_2 = DelayTime::create(0.5f);
    auto delay_3 = DelayTime::create(10.0f);

    auto fade_1 = FadeTo::create(1.2f, 210);
    auto fade_2 = FadeTo::create(0.7f, 150);
    auto fade_3 = FadeTo::create(0.7f, 255);


    //background->runAction(Sequence::create(delay_1, fade_1, nullptr));
    //startButton->runAction(Sequence::create(/*delay_1, delay_2,*/ fade_2, nullptr));
    label->runAction(Sequence::create(/*delay_1, delay_2,*/ fade_3, nullptr));


//    auto seqDimming = Sequence::create( move, delay, move_ease_back, nullptr);
    //runAction(RepeatForever::create(seqDimming));

    //startButton->setTitleText("Start");
    auto fade1 = FadeOut::create(2.0f);
    auto fade = FadeTo::create(2.0f, 100);
    //Sprite* mySprite = Sprite::create("SkinStartBTN.png");
    auto seq1 = Sequence::create(fade1, fade, nullptr);
    //startButton->runAction(seq1);


    //startButton->setTitleLabel(label);
    //startButton->setTitleFontSize(startButton->getTitleFontSize() * 3.7);
    startButton->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 1.5 + origin.y));
    startButton->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type)
    {

        //startButton->getLab
        //startButton->setOpacity(255);

        if (type == cocos2d::ui::Widget::TouchEventType::ENDED){
            auto sceneGame = SceneGame::create();
            Director::getInstance()->replaceScene(sceneGame);
            //get required scale by width(0.6);
            //get required scale by height;
        }
        //startButton->runAction(FadeTo::create(0.05f, 255) );
    });
    // add the button as a child to this layer
    this->addChild(startButton, 2);

    return true;
}




void SceneMenu::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    //Director::getInstance()->end();
    auto sceneGame = SceneGame::create();
    Director::getInstance()->replaceScene(sceneGame);

    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() as given above,instead trigger a custom event created in RootViewController.mm as below*/

    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);
}

/*
public function loadImage(_url:String):void
{
    // I got the variable _url and packing away the razor blades
}*/

