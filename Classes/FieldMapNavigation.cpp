//
// Created by rion on 02.03.21.
//

#include "FieldMapNavigation.h"
//using namespace std;

FieldMapNavigation::FieldMapNavigation(Snakes* snakes)
{
    this->snakes = snakes;
    mapWidth = snakes->mapWidth;
    mapHeight = snakes->mapHeight;
    availableMap2DInTime = false;//
    map2DInTime = new ContainSquare*** [maxTimeLayers];
    for (int i = 0; i < maxTimeLayers; i++)
    {
        map2DInTime[i] = new ContainSquare** [mapHeight];
        for (int j = 0; j < mapHeight; j++)
        {
            map2DInTime[i][j] = new ContainSquare* [mapWidth];
            for (int c = 0; c < mapWidth; c++)
            {
                map2DInTime[i][j][c] = nullptr;
            }
        }
    }
    //
    maxLength = snakes->mapHeight;
    stepsToLengthUp = snakes->timeGrowth / snakes->timeStepNormalSpeed;
    //auto d = Director::getInstance()->getDPI();
    //int d = cocos2d::Device::getDPI();
    //run action to initialize map in time
    //return true;


}

FieldMapNavigation::~FieldMapNavigation()
{

}


bool FieldMapNavigation::updateDirections(int curTimeInterval) // update render
{
    //змеи вызывают для получения маршрута
    // этот метод обновлеяет posHeadInTime в соответствии со свжими данными от snakes->snakes
    //также создаются новые змеи, если это первый ход, либо  прошло определенное время (НЕТ )
    //run randomizer for determinate position of first snake
    Pos* snakePos = getRandPosForSnake();
    //create new Snake
    //add new snake to posHeadInTime and to map2DInTime
    posHeadInTime[0] = new Pos*[maxTimeLayers];
    posHeadInTime[0][0] = snakePos; //[id] [layers]
/*
    for (int i = 1; i < max)
    {
        //method get rand next pos //returned vector of positions
        //get rand direction for snake with id
        //get rand count step in direction
    }*/

    //posHeadInTime.erase(0);
    //select direction for new snake
    //choose the number of step in a given direction
    //prognosis 15 - 20 steps
    //getRandPosForSnake()

    delete snakePos;

    this->curTimeInterval = curTimeInterval % maxTimeLayers;// time is cyclical
    return true;
}

std::vector<Pos*> FieldMapNavigation::getNewSnake()
{
    std::vector<Pos*> snakeNavigation; //нужно хранить по строка/столбец, для простой навигации (нет, это будет мешать сохранять данные в общий массив)
    //get rand start pos
    snakeNavigation.push_back( getRandPosForSnake() );
    int i = 0;
    do{ //cycle get rand direction
        getRandDirection( snakeNavigation[i], snakeNavigation);
        //get possible steps in direction
        //auto d = Director::getInstance() ->getDPI();
        //auto d = cocos2d::Device::get;
        //DisplayMetrics dm = new DisplayMetrics();
        //cocos2d::Director::getOpenGLView()::get
        //get rand count step in direction

        //i += count step in diraction
    }
    while(i < minTimeLayers);

    return snakeNavigation;
}

Pos* FieldMapNavigation::getRandPosForSnake()
{
    //Pos randPos = {rand() % mapWidth, rand() % mapHeight};
    Pos* randPos = new Pos {rand() % mapWidth, rand() % mapHeight};
    //will be added check if there no flowers nearby
    return randPos;
}
Direction FieldMapNavigation::getRandDirection(const Pos* posHead, const std::vector<Pos*> tempNavigation)
{
    //Direction t_Direction = static_cast<Direction>(random % 5);

    //0123 //0123
    Pos posDirections[4] = {Pos{posHead->x,posHead->y+1}, Pos{posHead->x,posHead->y-1}, Pos{posHead->x-1,posHead->y}, Pos{posHead->x+1,posHead->y}}; //up down left right
    //GET AVAILABLE DIRECTION
    //check out of playing field
    for (int i = 0; i < 4; i++)
    {
        if (posDirections[i].x < 0 || posDirections[i].x > mapWidth -1 || posDirections[i].y < 0 || posDirections[i].y > mapHeight -1 )
        {
            posDirections[i].x = -1;
            posDirections[i].y = -1;
        }
    }
    // checking that snake not crashes into its own temp tail
    int tSize = tempNavigation.size();
    for (int i = 0; i < tSize; i++) // checking that snake not crashes into its own tail
    {
        for (int j = 0; j < 4; j++)
        {
            if (posDirections[j].x > 0 && posDirections[j].x == tempNavigation[i]->x && posDirections[j].y == tempNavigation[i]->y )
            {
                posDirections[j].x = -1;
                posDirections[j].y = -1;
            }
        }
    }
    // checking map2DInTime with tail length check for time collision //time][row][column][data field contain snakeID]
    //get max possible length snake in future(10 steps max) //the snake probably won't grow 10 divisions so fast
    unsigned snakeID = 0;
    //нужно иметь массив со змеями,где можно смотреть их реальные свойства по id
    int maxSnakeLength = howFastSnakeLeavePos(posDirections[0], 1, snakeID );//how quickly the snake will crawl away and other snakes will not crash into it

    int direction = rand() % 4; // new direction for snake
    Pos nextPos = Pos{1,2};
    //
    int fail_directions[4];
    int fail_counter = 0;
    //

    bool emptyField = true;//is possible turn in direction
    int numD = direction;
    int j = 0;
    do {//checking direction //нужно сразу проверить все 4 направления, а потом брать рандом и проверять
        emptyField = true;
        for (int i = 0; i < tSize; i++) // checking that snake not crashes into its own tail
        {
            if ( nextPos.x == tempNavigation[i]->x && nextPos.y == tempNavigation[i]->y )
            {
                emptyField = false;
                break;
            }
        }
        //
        for (int i = 0; i < tSize; i++) // checking that snake not crashes others snakes in time
        {
        }

        //
        if (!emptyField)
        {
            direction = ++numD % 4;
            j++;
        }
    }
    while (emptyField == false && j < 4); // 9->4, 5, 6, 7

    //
    if (emptyField)
    {
        return static_cast<Direction>(direction);
    }
    direction++;
    emptyField = true;

    //чекать во времени то, что есть, и то, что хотим добавить
    /*
    while ( tempNavigation[][] )
    {
        int tDir = (random % 4);
        if ()
        {

        }
    }*/

    return static_cast<Direction>(direction);
}

int FieldMapNavigation::howFastSnakeLeavePos(Pos posCheck, int stepsToPosCheck, unsigned snakeID )
{


    return 3;
}

void FieldMapNavigation::addNewSnake(int timeIntervalForCreate, unsigned id)
{
    ......
}
