//
// Created by rion on 01.03.21.
// BUSINESS LOGIC

#ifndef PROJ_ANDROID_PLAYAREA_H
#define PROJ_ANDROID_PLAYAREA_H
#include "cocos2d.h"
#include "TopGamePanel.h"
#include "Snakes.h"

class PlayArea : public cocos2d::Node {
    PlayArea();
    bool init();

    TopGamePanel* topMenu; //draw game menu
    Snakes* snakesFied; //draw snakes

public:
    virtual ~PlayArea();
    static PlayArea* create();

    void StartLog();


};


#endif //PROJ_ANDROID_PLAYAREA_H
