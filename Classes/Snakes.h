//
// Created by rion on 02.03.21.
//

#ifndef PROJ_ANDROID_SNAKES_H
#define PROJ_ANDROID_SNAKES_H
#include "cocos2d.h"
#include "CommonPropertiesOfSnakes.h"
#include "FieldMapNavigation.h"

class FieldMapNavigation;

struct Snake{
    unsigned id;
    int length;//current length snake
    Pos* arrPos;//first position is the head //always 10 elements(max length)
};

USING_NS_CC;

//PlayArea должна быть контейнером для Snakes - так,чтобы смещение на экране для PlayArea - также смещало змей
class Snakes : public cocos2d::Node {
    friend class FieldMapNavigation;
    FieldMapNavigation* mapNavigation; //map navigation for snakes
    //void startDrawing(); // whren mapNavigation is ready then -> mapNavigation trigger Snakes //multithread

    int startLength = 2; //new snake have fixed length (2 part)
    int normalLength = 5;
    int maxLength = 10;

    Snakes();

    EventListenerTouchAllAtOnce* touchListener;

    bool init(Size pxSize);
    //BUSINESS PROPERTIES
    float timeOfCurStep = 0;//time after start of step (sec)
    float absolutelyTimeOfCurStep = 0;//time in seconds
    unsigned curTimeInterval = 0; //current step number
    float timeStepNormalSpeed = 1; //maximum duration of one step (sec)
    float timeStepFastSpeed = 0.5; //minimum duration of one step (sec)
    float timeStepCurSpeed = 1;
    float timeGrowth = 0.5;//growth time per one division in seconds
    float timeToCreateNewSnake = 2; //how many need seconds to ad new snake to the Field
    unsigned counterOfSnakes = 0;// counter of snakes for getting id of new snake
    float timeFastModeAtOneFlower = 5; //how much time at fast mode at every flowers eating
    float timeAtFastMode = 0; //how much time at fast mode after eating flowers
    float spendTimeAtFastMode = 0; // when the spend time is equals timeAtFastMode then -> timeAtFastMode and spendTimeAtFastMode reset to zero
    //
    //FIELD PROPERTIES
    int mapWidth;//number of parts across the width of the field
    int mapHeight;//number of parts across the height of the field
    float widthOfPieceOfMap;//width in pixels of one map part
    //centering
    //float offset_x;
    //float offset_y;
    //Sprite* sprite;
    DrawNode* drawPolygone;
    Size pxSize;//the size of the playing field with snake in pixels
    int areaWidth = 16;//the width of the playing field in areas (how many steps in width)
    int areaHeight;
    bool pause;
    //SNAKES
    std::map <unsigned, Snake*> snakes;//containing data with real state of snakes for drawing and tracking
    void addSnake(unsigned id); //initiated by FieldMapNavigation
    int yt=0;



public:

    virtual ~Snakes();
    static Snakes* create(Size pxSize); //относительные размеры игрового поля, размеры игрового поля в пикселях(для масштаба),
    // относительная позиция на игровом поле // id // массив ходов // скорость движения (тек&мкс) // скорость роста // начальная длинна & текущая возможная длинна & мак длина
    // индикатор о текущем номере хода, и сколько времени уже прошло с его начала
    // индикатор о том, что нужно расти до максимума
    // индикатор о том, что нужно применить ускорение (это передавать вместе с номером хода)
    //CommonPropertiesOfSnakes
    //method draw
    //move(x,y)
    //getId
    virtual void draw(Renderer *renderer, const Mat4& transform, uint32_t flags);
    virtual void update(float delta);
};


#endif //PROJ_ANDROID_SNAKE_H
