//
// Created by rion on 01.03.21.
// BUSINESS LOGIC

#include "PlayArea.h"
#include <iostream>

PlayArea::PlayArea()
{
}

PlayArea::~PlayArea()
{}

PlayArea* PlayArea::create()
{
    PlayArea *playArea = new PlayArea();
    if (playArea && playArea->init())
    {
        playArea->autorelease();
        return playArea;
    }
    CC_SAFE_DELETE(playArea);
    return nullptr;
}

bool PlayArea::init()
{
    snakesFied = Snakes::create(Director::getInstance()->getVisibleSize());
    snakesFied->setAnchorPoint(Vec2(0, 0));
    snakesFied->setPosition(Vec2(0, 0));
    this->addChild(snakesFied, 0);
    return true;
}

//


void PlayArea::StartLog(){
    //print(std::cout, name, "-- Error writing to log file. --");
    //std::cout << "My name is Anton.";
    //log("DEBUG: PlayArea ");
    cocos2d::log("testing1234567");
}
